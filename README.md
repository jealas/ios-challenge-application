# Various Objects - iOS App Challenge #

### What is this repository for? ###

* This repository presents a simple iOS application that we will use in order to asses the expertise and level of understanding of our potential iOS interns and partners.
* Version 1.0

### What to do? ###
1. Create a GIT repository for your (Objective-C and/or Swift) iOS application.
2. Use whatever editor or IDE you see fit to implement the application.
3. Publish your GIT repository online and send us a link.
4. Provide build instructions so we can test your application. Please note that we will test your application using various iOS devices and emulators.
5. Ask us questions! We would love to hear your questions and input!

### Implementation notes ###

![mockup-iPhone_6.png](https://bitbucket.org/repo/x5dqon/images/1116266963-mockup-iPhone_6.png)

* The task is to create an application that makes a simple REST API call to our back-end for "food and beverage" locations around a user's geographic location.
* The UI consists of a list of places organized into the neighborhoods they fall in and a search bar where the user can enter a search string (ex. Burgers, Pizza, Chipotle etc.)
* The places are sorted in ascending order (where the first place is the closest to the user).
* If no search string is provided (ex. on the application start up), the application should query for places around the user's current location.
* The user should also be able to search for places given a search string and their current location.
* You may notice that the REST API returns details that are not needed; this is OK and is done on purpose.
* Below you will find a description of the REST APIs you will need to use and a link with assets for the application.
* Note: The REST APIs will return a list of places around the user (even if they are not "food and beverages" places). Only display places with a category id in this list:
    * 312 313 314 315 316 338 339 340 341 342 343 344 345 346 347 348 349 350 351 352 353 354 355 356 357 358 359 360 361 362 363 364 365 366 367 368 457 458 464

### REST API notes ###

* Server IP: 54.69.89.99
* Port: 9090
* Session Key: gN9NoIZikqscf8RAkcQh
* Session Secret: 0K9LIxOMVbdsSGW9EF6X
* Note: Replace the variables surrounded by {{ }} with actual values.

**Getting places data given a Latitude and Longitude**

* Endpoint: /location/getRawPlaces
* Headers:
    * Content-Type: application/json
    * X-Current-Session-Key: {{SESSION_KEY}}
    * X-Current-Message-Sig: {{MESSAGE_SIGNATURE}}

* Example Body:

```
#!json

    {
        "lat": 33.6404950,
        "lon": -117.8442960
    }
```


**Getting places data given a Latitude and Longitude plus a query string**

* Endpoint: /location/getRawPlacesForQuery
* Headers:
    * Content-Type: application/json
    * X-Current-Session-Key: {{SESSION_KEY}}
    * X-Current-Message-Sig: {{MESSAGE_SIGNATURE}}

* Example Body:

```
#!json

    {
        "latLonLocation": {
            "lat": 33.6404950,
            "lon": -117.8442960
        },

        "queryString": "Burger"
    }
```


**Message Signatures**

* Message Signatures are computed by taking the HMAC SHA-256 bit hash of the request body and the session secret.
* Then the Base64 string of the hash is sent together with the request.
* Pseudo Code: to_base64(hmac_sha256(request.body, session.secret)).

* Request Bodies are encoded as UTF-8.

### Resources ###
[Application Assets](https://www.dropbox.com/s/1iimy5714rsl8mi/Current%20-%20Code%20Challenge%20Assets.zip?dl=0)

Optional: Download the Mac OSX Sketch application to get a more detailed view of the UI (Sketch file included in assets): [Sketch App](https://www.sketchapp.com/)

### Who do I talk to? ###

* Questions about the UI: Darren Barone <darren@variousobjects.com>
* Questions about the Back-end: Jesse Alas <jesse@variousobjects.com>